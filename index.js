const http = require('http');
const path = require('path');
const fs = require('fs');

const server = http.createServer((req, res) => {
    const potDoDatoteke = path.join(__dirname, 'public', req.url === '/' ? 'index.html' : req.url);

    const koncnica = path.extname(potDoDatoteke);

    let contentType = 'text/html';

    switch (koncnica) {
        case '.json':
            contentType = 'application/json';
            break;
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.png':
            contentType = 'image/png';
            break;
        case '.css':
            contentType = 'text/css';
            break;
    }

    fs.readFile(potDoDatoteke, (err, data) => {
        if (err) {
            if (err.code === 'ENOENT') {
                fs.readFile(path.join(__dirname, 'public', '404.html'), (err1, data1) => {
                    res.writeHead(200, { 'Content-type': 'text/html' });
                    res.end(data1, 'utf8');
                });
            }
        } else {
            res.writeHead(200, { 'Content-type': contentType });
            res.end(data, 'utf8');
        }
    });


    // console.log(potDoDatoteke);

    // if (req.url === '/' || req.url === '/index.html') {
    //     res.writeHead(200, { 'Content-type': 'text/html' });
    //     res.end('<html><head><body><h1>Index</h1></body></html>')
    // }

    // if (req.url === '/about.html') {
    //     res.writeHead(200, { 'Content-type': 'text/html' });
    //     res.end('<html><head><body><h1>About</h1></body></html>')
    // }

    // if (req.url === '/contact.html') {
    //     res.writeHead(200, { 'Content-type': 'text/html' });
    //     res.end('<html><head><body><h1>Contact form</h1></body></html>')
    // }

    // if (req.url === '/api/students') {
    //     const students = [
    //         { ime: 'Boštjan', priimek: 'Šumak', vpisna: '9349349439' },
    //         { ime: 'Janez', priimek: 'Novak', vpisna: '4353454534' },
    //         { ime: 'Mirko', priimek: 'Krajnc', vpisna: '6776776' }
    //     ];
    //     res.writeHead(200, { 'Content-type': 'application/json' });
    //     res.end(JSON.stringify(students));

    // }
    // res.end();
});

const PORT = process.env.PORT || 5000;

server.listen(PORT, () => {
    console.log(`Strežnik posluša na http://localhost:${PORT}`);
});